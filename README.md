# GoldED+ Fidonet Message Editor
[![Build Status](https://travis-ci.org/golded-plus/golded-plus.svg?branch=master)](https://travis-ci.org/golded-plus/golded-plus)
[![Build status](https://ci.appveyor.com/api/projects/status/14d2qulas4ti4m1v/branch/master?svg=true)](https://ci.appveyor.com/project/dukelsky/golded-plus/branch/master)


**GoldED+** is a multiplatform Fidonet message editor. It can be used in 
Windows and Unix-like operating systems (Linux, *BSD, Mac OS X). One may 
use it to read, create and edit both netmail and echomail messages.

